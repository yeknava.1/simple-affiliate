# Laravel Simple Affiliate Package

Laravel Simple Affiliate Package.

## Features

-	Utm code
-	Publisher balance
-	Promoted Logs
-	Rate limit

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple affiliate package.

```bash
composer require yeknava/simple-affiliate
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

Add AffiliatePublisher trait to user model.

```php
<?php

use Yeknava\SimpleAffiliate\AffiliatePublisher;

class User extends Model {
    use AffiliatePublisher;

    protected static function boot()
    {
        parent::boot();
        self::publisherBoot();
    }
}
```
Add AffiliatePublishable trait to publishable (purchasable) models.

```php
<?php

use Yeknava\SimpleAffiliate\AffiliatePublishable;

class ItemModel extends Model {
    use AffiliatePublishable;
}
```

```php
$user1 = new UserModel([]);
$user1->save();

$code = $user1->utmCode();
$user1->publisherBalance(); // 0

$item = new ItemModel([]);
$item->save();

$data = [
    'source' => 'google',
    'medium' => 'email',
    'campaign' => 'spring-sale',
    'content' => 'footer-button',
    'type' => 'paid',
    'extra' => [
        'user_id' => $user1->id
    ]
];

$item->promoted($code, $data, 1000, 1); // with 1 percent share (10 unit share for publisher)

$user1->publisherBalance(); // 10

$item->promoted($code, $data, 100); // with default share
```
## Config
```php
<?php

use Yeknava\SimpleAffiliate\Samples\WalletServiceSample;

return [
    'default_share' => 5,

    'dedicate_wallet_service' => null, //WalletServiceSample::class

    // seconds needs to passed before each new log
    // could be save with same utm code and item
    'rate_limit' => 0,

    'rate_limit_agent' => false,

    'rate_limit_throw_exception' => false,

    'create_utm_on_save' => true,

    // if create_utm_on_fetch be true, utm will be created if publisher has not any utm
    // useful when app does not have this package from first day of it's launch
    'create_utm_on_fetch' => true,

    'codes_table' => 'utm_codes',

    'logs_table' => 'utm_logs'
];
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
