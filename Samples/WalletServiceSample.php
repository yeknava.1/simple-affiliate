<?php

namespace Yeknava\SimpleAffiliate\Samples;

use Yeknava\SimpleAffiliate\WalletServiceInterface;
use App\User;

class WalletServiceSample implements WalletServiceInterface {
    public function increaseBalance(
        int $publisherId,
        int $itemId,
        float $amount,
        int $share
    ) : void {
        $user = User::Find($publisherId);
        $user->increaseBalance(($amount*$share)/100);
    }
}