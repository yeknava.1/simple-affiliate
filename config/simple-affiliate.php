<?php

use Yeknava\SimpleAffiliate\Samples\WalletServiceSample;

return [
    'default_share' => 5,

    'dedicate_wallet_service' => null, //WalletServiceSample::class

    // seconds needs to passed before each new log
    // could be save with same utm code and item
    'rate_limit' => 0,

    'rate_limit_agent' => false,

    'rate_limit_throw_exception' => false,

    'create_utm_on_save' => true,

    // if true, utm will be created if publisher has not any utm
    // its useful when app does not have this package from first day of it's launch
    'create_utm_on_fetch' => true,

    'codes_table' => 'utm_codes',

    'logs_table' => 'utm_logs'
];
