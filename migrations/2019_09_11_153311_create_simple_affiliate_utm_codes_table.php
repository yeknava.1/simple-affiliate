<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleAffiliateUtmCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-affiliate.codes_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->nullableMorphs('publisher');
            $table->nullableMorphs('item');
            $table->string('code')->unique();
            $table->tinyInteger('share')->unsigned()->nullable();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->double('balance', 16, 4)->default(0);
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-affiliate.codes_table'));
    }
}
