<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleAffiliateCodeLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-affiliate.logs_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('code_id')->unsigned()->nullable()->index();
            $table->foreign('code_id')->references('id')->on(config('simple-affiliate.codes_table'));
            $table->nullableMorphs('item');
            $table->double('amount', 16, 4)->nullable();
            $table->double('publisher_share', 16, 4)->nullable();
            $table->string('content')->nullable();
            $table->string('source')->nullable();
            $table->string('medium')->nullable();
            $table->string('campaign')->nullable();
            $table->string('term')->nullable();
            $table->string('type')->nullable();
            $table->string('ip')->nullable();
            $table->string('agent')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-affiliate.logs_table'));
    }
}
