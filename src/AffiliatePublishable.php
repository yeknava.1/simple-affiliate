<?php

namespace Yeknava\SimpleAffiliate;

use Carbon\Carbon;
use Yeknava\SimpleAffiliate\Exceptions\UtmCodeNotFoundException;

trait AffiliatePublishable {
    public function utmCodes(string $code = null, bool $showExpired = true)
    {
        $utm = $this->morphMany(SimpleUtm::class, 'item');
        if (!$showExpired) {
            $utm = $utm->whereNull('expired_at')
                ->orWhere('expired_at', '>', Carbon::now());
        }
        if ($code) {
            $utm = $utm->where('code', $code);
        }

        return $utm;
    }

    public function utmLogs(string $code = null)
    {
        if ($code) {
            $utmCode = $this->utmCodes($code)->first();
            if (!$utmCode) {
                throw new UtmCodeNotFoundException();
            }
            return $utmCode->logs();
        }
        return $this->morphMany(SimpleUtmLogs::class, 'item');
    }

    public function promoted(
        string $code,
        array $data = [],
        float $amount = null,
        int $share = null
    ) :SimpleUtmLogs {
        return SimpleUtmLogs::makeNew($code, $this, $data, $amount, $share);
    }

    public function newViewLog(
        string $code,
        array $data = []
    ) :SimpleUtmLogs {
        $data['type'] = 'view';
        return $this->promoted($code, $data);
    }

    public function newPaidLog(
        string $code,
        array $data = [],
        float $amount = null,
        int $share = null
    ) :SimpleUtmLogs {
        $data['type'] = 'paid';
        return $this->promoted($code, $data, $amount, $share);
    }
}