<?php

namespace Yeknava\SimpleAffiliate;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;
use Yeknava\SimpleAffiliate\Exceptions\UtmCodeNotFoundException;

trait AffiliatePublisher {
    
    protected static function publisherBoot()
    {
        parent::boot();

        if (config('simple-affiliate.create_utm_on_save')) {
            static::created(function ($model) {
                SimpleUtm::makeNew([], $model);
            });
        }

        static::deleted(function ($model) {
            $model->utmCodes()->delete();
        });

        if (method_exists(__CLASS__, "restore")) {
            static::restored(function ($model) {
                $model->utmCodes()->restore();
            });
        }
    }

    public static function findPublisherByCode(string $code) : Model
    {
        $utm = SimpleUtm::where('code', $code)->first();
        if (!$utm) throw new UtmCodeNotFoundException();

        return $utm->publisher;
    }

    public function utmCodes(string $code = null, bool $showExpired = true)
    {
        $utm = $this->morphMany(SimpleUtm::class, 'publisher');
        if (!$showExpired) {
            $utm = $utm->whereNull('expired_at')
                ->orWhere('expired_at', '>', Carbon::now());
        }
        if ($code) {
            $utm = $utm->where('code', $code);
        }

        return $utm;
    }

    public function utmCode(bool $showExpired = true) : ?string
    {
        $utm = $this->morphMany(SimpleUtm::class, 'publisher');
        if (!$showExpired) {
            $utm = $utm->whereNull('expired_at')
                ->orWhere('expired_at', '>', Carbon::now());
        }

        $utm = $utm->orderBy('id', 'desc')->select('code')->first();
        if (empty($utm) && config('simple-affiliate.create_utm_on_fetch')) {
            $utm = $this->createUtmCode([]);
        }

        return $utm->code ?? null;
    }

    public function publisherBalance(string $code = null) : ?float
    {
        if ($code) {
            $utm = $this->utmCodes($code)->select('balance')->first();
            if (!$utm) throw new UtmCodeNotFoundException();

            return $this->utmCodes($code)->balance;
        }

        return $this->utmCodes()->select('balance')->first()->balance ?? null;
    }

    public function createUtmCode(array $data, Model $item = null, string $code = null) {
        return SimpleUtm::makeNew($data, $this, $code, $item);
    }

    public function withdrawBalance(string $code = null, float $amount = null) : float {
        try {
            DB::beginTransaction();
            $utm = $this->utmCodes($code)->lockForUpdate()->first();
        
            if (!$utm) throw new UtmCodeNotFoundException();

            $utm->balance -= $amount ?? $utm->balance;
            $utm->save();

            DB::commit();
            return $utm->balance;
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }
}