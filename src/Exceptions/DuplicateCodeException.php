<?php

namespace Yeknava\SimpleAffiliate\Exceptions;

class DuplicateCodeException extends SimpleAffiliateException {
}