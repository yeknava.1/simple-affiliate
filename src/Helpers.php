<?php

namespace Yeknava\SimpleAffiliate;

class Helpers {
    public static function newCode() : string {
        $code = uniqid(random_int(1000, 9999));
        $utm = SimpleUtm::where('code', $code)->first();
        if ($utm) {
            return self::newCode();
        }

        return $code;
    }
}