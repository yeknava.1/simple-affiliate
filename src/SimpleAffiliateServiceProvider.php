<?php

namespace Yeknava\SimpleAffiliate;

use Illuminate\Support\ServiceProvider;

class SimpleAffiliateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            dirname(__DIR__, 1).'/config/simple-affiliate.php' =>
                $this->app->configPath().'/simple-affiliate.php',

            dirname(__DIR__, 1) . '/migrations/' =>
                database_path('migrations'),
        ]);
    }
}
