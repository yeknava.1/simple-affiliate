<?php

namespace Yeknava\SimpleAffiliate;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleAffiliate\Exceptions\DuplicateCodeException;

class SimpleUtm extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-affiliate.codes_table');
    }

    protected $fillable = [
        'title', 'slug', 'expired_at'
    ];

    protected $casts = [
        'extra' => 'array',
        'expired_at' => 'datetime'
    ];

    public function publisher()
    {
        return $this->morphTo();
    }

    public function item()
    {
        return $this->morphTo();
    }

    public function logs()
    {
        return $this->hasMany(SimpleUtmLogs::class, 'code_id');
    }

    public static function makeNew(
        array $data,
        Model $publisher = null,
        string $code = null,
        Model $item = null
    ) : self {
        if ($code) {
            $utm = self::where('code', $code)->first();
            if ($utm) {
                throw new DuplicateCodeException();
            }
        } else {
            $code = Helpers::newCode();
        }
        $utm = new self($data);
        $utm->code = $code;
        $utm->balance = 0;
        $utm->share = $data['share'] ?? config('simple-affiliate.default_share');
        if ($item) {
            $utm->item()->associate($item);
        }
        if ($publisher) {
            $utm->publisher()->associate($item);
        }
        $publisher->utmCodes()->save($utm);

        return $utm;
    }
}
