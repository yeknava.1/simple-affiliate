<?php

namespace Yeknava\SimpleAffiliate;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ReflectionClass;
use Throwable;
use Yeknava\SimpleAffiliate\Exceptions\InvalidWalletServiceException;
use Yeknava\SimpleAffiliate\Exceptions\RateLimitException;
use Yeknava\SimpleAffiliate\Exceptions\UtmCodeNotFoundException;

class SimpleUtmLogs extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-affiliate.logs_table');
    }

    protected $fillable = [
        'content', 'source', 'type',
        'medium', 'campaign', 'term',
        'extra'
    ];

    protected $casts = [
        'extra' => 'array',
    ];

    public function utmCode()
    {
        return $this->belongsTo(SimpleUtm::class, 'code_id');
    }

    public function item()
    {
        return $this->morphTo();
    }

    public static function makeNew(
        string $code,
        Model $item = null,
        array $data = [],
        float $amount = null,
        int $share = null
    ) :self
    {
        $request = new Request();
        $ip = $request->getClientIp();
        $agent = $request->userAgent();

        $seconds = config('simple-affiliate.rate_limit');
        if ($seconds > 0) {
            $key = 'simple_affiliate.log.'.$code.$ip;
            if($item) $key .= $item->getKey();
            if ($agent = config('simple-affiliate.rate_limit_agent')) {
                $key .= $agent;
            }
            
            $log = app('cache')->get($key);
            if(!empty($log)) {
                if(config('simple-affiliate.rate_limit_throw_exception')) {
                    throw new RateLimitException();
                }

                return $log;
            }
        }

        $utmCode = SimpleUtm::where('code', $code)
                    ->where(function (Builder $query) {
                        return $query->whereNull('expired_at')
                            ->orWhere('expired_at', '>', Carbon::now());
                    });

        if ($amount) {
            $utmCode = $utmCode->lockForUpdate();
        }
        
        try {
            DB::beginTransaction();
            $utmCode = $utmCode->first();
    
            if (!$utmCode) throw new UtmCodeNotFoundException();
    
            $log = new self($data);
            $log->ip = $ip;
            $log->agent = $agent;
            if ($amount) {
                $publisherShare = $share ? ($amount*$share)/100 : ($amount*$utmCode->share)/100;

                $log->amount = $amount;
                $log->publisher_share = $publisherShare;
            }
            if ($item) $log->item()->associate($item);
            $log->utmCode()->associate($utmCode);
            $log->save();
    
            if ($amount) {
                $utmCode->balance += $publisherShare;
                $utmCode->save();

                $walletService = config('simple-affiliate.dedicate_wallet_service');

                $service = new ReflectionClass($walletService);

                if (!$service->implementsInterface(WalletServiceInterface::class)) {
                    throw new InvalidWalletServiceException();
                }

                $service->newInstance()->increaseBalance(
                    $utmCode->publisher_id,
                    $item->getKey() ?? null,
                    $amount,
                    $share ?? $utmCode->share
                );
            }
            DB::commit();

            if ($seconds > 0) {
                app('cache')->put($key, $log, Carbon::now()->addSeconds($seconds));
            }
            
            return $log;
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

}
