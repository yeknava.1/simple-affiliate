<?php

namespace Yeknava\SimpleAffiliate;

interface WalletServiceInterface {
    public function increaseBalance(
        int $publisherId,
        ?int $itemId,
        float $amount,
        float $share
    ) : void;
}