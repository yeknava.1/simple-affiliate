<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleAffiliate\AffiliatePublishable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModel extends Model
{
    use AffiliatePublishable, SoftDeletes;

    protected $table = 'simple_affiliate_test_items';
}
