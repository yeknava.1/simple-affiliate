<?php

use Orchestra\Testbench\TestCase;
use Tests\UserModel;
use Tests\ItemModel;

class SimpleTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp() : void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('simple-affiliate', require_once(__DIR__ . '/../config/simple-affiliate.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleAffiliate\SimpleAffiliateServiceProvider::class,
        ];
    }

    public function test()
    {        
        $user1 = (new UserModel([]));
        $user1->save();

        $user2 = (new UserModel([]));
        $user2->save();

        $code = $user1->utmCode();
        $this->assertIsString($code);
        $this->assertNotEquals($code, $user2->utmCode());
        $this->assertEquals(count($user1->utmCodes), 1);
        $this->assertEquals($user1->publisherBalance(), 0);

        $item = (new ItemModel([]));
        $item->save();

        $data = [
            'source' => 'google',
            'medium' => 'email',
            'campaign' => 'spring-sale',
            'content' => 'footer-button',
            'type' => 'paid',
            'extra' => [
                'user_id' => $user1->id
            ]
        ];

        $item->promoted($code, $data, 100);
        $item->promoted($code, $data, 1000, 1);

        $this->assertEquals($user1->publisherBalance(), 15);

        $this->assertEquals($user1->id, UserModel::findPublisherByCode($code)->id);
        
        $this->assertEquals($user1->utmCodes()->first()->logs()->count(), 2);
    }
}