<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleAffiliate\AffiliatePublisher;
use Yeknava\SimpleAffiliate\AffiliatePublishable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model
{
    use AffiliatePublisher, SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        self::publisherBoot();
    }

    protected $table = 'simple_affiliate_test_users';
}
